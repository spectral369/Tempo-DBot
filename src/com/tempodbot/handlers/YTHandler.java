package com.tempodbot.handlers;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.TrackMarker;
import com.sedmelluq.discord.lavaplayer.track.TrackMarkerHandler;
import com.tempodbot.mediaqueue.MediaItem;
import com.tempodbot.mediaqueue.MediaQueue;
import com.tempodbot.statics.EmbeddedMessage;
import com.tempodbot.utils.YTSearch;

import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;

public class YTHandler implements AudioLoadResultHandler {
	AudioPlayer audioPlayer;
	MessageChannel txtChannel;
	private List<MediaItem> queue;

	private static final long CROSSFADE_BEGIN = TimeUnit.SECONDS.toMillis(5);
	private static final long CROSSFADE_PRELOAD = CROSSFADE_BEGIN + TimeUnit.SECONDS.toMillis(3);

	public YTHandler(AudioPlayer player, MessageChannel messageChannel, List<MediaItem> queue) {
		this.audioPlayer = player;
		this.txtChannel = messageChannel;
		this.queue = queue;
	}

	@Override
	public void trackLoaded(AudioTrack track) {
		applyMakers(track);
		audioPlayer.playTrack(track);

	}

	@Override
	public void playlistLoaded(AudioPlaylist playlist) {

		audioPlayer.playTrack(playlist.getTracks().getFirst());
		if (!queue.isEmpty())
			queue.remove(0);
		for (AudioTrack track : playlist.getTracks()) {
			MediaQueue list = YTSearch.getVideoDetails(track.getInfo().uri);
			MediaItem item = list.get(0);
			queue.add(item);
		}

	}

	@Override
	public void noMatches() {
		txtChannel.sendMessageEmbeds(EmbeddedMessage.MessageEmbed("Nope, no matches.")).queue();
		queue.clear();

	}

	@Override
	public void loadFailed(FriendlyException exception) {
		queue.clear();
		txtChannel
				.sendMessageEmbeds(
						EmbeddedMessage.MessageEmbed("Error", "Hujove tuke treba da pravit vija\n " + exception))
				.queue();

	}

	public void stop() {
		audioPlayer.stopTrack();
	}

	private static void applyMakers(AudioTrack track) {
		final TrackMarkerHandler xfadeLoadHandler = (TrackMarkerHandler.MarkerState state) -> {
			if (state == TrackMarkerHandler.MarkerState.REACHED) {
				System.out.println("Fade begin handler has been reached");
			}
		};

		final TrackMarkerHandler xfadeBufferHandler = (TrackMarkerHandler.MarkerState state) -> {
			if (state == TrackMarkerHandler.MarkerState.REACHED) {
				System.out.println("Buffer begin handler has been reached");
			}
		};

		track.addMarker(new TrackMarker(track.getDuration() - CROSSFADE_BEGIN, xfadeLoadHandler));
		track.addMarker(new TrackMarker(track.getDuration() - CROSSFADE_PRELOAD, xfadeBufferHandler));
	}

}
