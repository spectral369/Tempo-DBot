package com.tempodbot.statics;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author spectral369
 * @description Static info class Here we should put static information like
 *              static Strings/configs
 * 
 * 
 */
public enum StaticInfo {

	DISCORD_TOKEN(""), PO_TOKEN(""), VISITOR_DATA("");

	private static final Map<String, StaticInfo> DATA = new HashMap<>();

	private String value;
	private String poToken;
	private String visitorData;

	StaticInfo(String value) {
		
		try {

			File tokenFile = new File(System.getProperty("user.home") + File.separator + "token.txt");
			if (!tokenFile.exists()) {
				FileWriter fw = new FileWriter(tokenFile);
				fw.write("<Insert token here>");
				fw.write("<po_token here>");
				fw.write("<visitor_data here>");
				fw.close();
			} else {

				List<String> extractedLines = Files.readAllLines(Paths.get(tokenFile.toURI()));

				this.value = extractedLines.get(0);
				this.poToken = extractedLines.get(1);
				this.visitorData = extractedLines.get(2);

			}
		} catch (IOException e) {

			System.out.println(e.getLocalizedMessage());
		}

	}

	static {
		for (StaticInfo e : values()) {
			DATA.put(e.toString(), e);

		}
	}

	public static String valueOfStr(StaticInfo name) {
		System.out.println(DATA.get(name.toString()).value);
		return DATA.get(name.toString()).value;

	}

	public String getVal() {
		return value;
	}

	public String getPoToken() {
		return poToken;
	}

	public String getVisitorData() {
		return visitorData;
	}

}
